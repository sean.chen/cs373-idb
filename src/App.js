import React from 'react';
import NavBar from './components/NavBar';
import Home from './components/Home';
import PowerPlants from './components/PowerPlants'
import PowerPlantMaddox from './components/instances/PowerPlantMaddox'
import PowerPlantMachoSprings from './components/instances/PowerPlantMachoSprings'
import PowerPlantMainCanalHeadworks from './components/instances/PowerPlantMainCanalHeadworks'
import Cities from './components/Cities'
import CityAlbuquerque from './components/instances/CityAlbuquerque'
import CityAtlanta from './components/instances/CityAtlanta'
import CityEphrata from './components/instances/CityEphrata'
import CityMinneapolis from './components/instances/CityMinneapolis'
import Organizations from './components/Organizations'
import OrganizationColumbiaBasinHydropower from './components/instances/OrganizationColumbiaBasinHydropower'
import OrganizationSouthernPowerCo from './components/instances/OrganizationSouthernPowerCo'
import OrganizationSouthwesternPublicServiceCo from './components/instances/OrganizationSouthwesternPublicServiceCo'
import Policies from './components/Policies'
import PolicySolarandWindEnergyDeviceFranchiseTaxDeduction from './components/instances/PolicySolarandWindEnergyDeviceFranchiseTaxDeduction'
import PolicySolarandWindEnergyBusinessFranchiseTaxExemption from './components/instances/PolicySolarandWindEnergyBusinessFranchiseTaxExemption'
import PolicyRenewableEnergySystemsPropertyTaxExemption from './components/instances/PolicyRenewableEnergySystemsPropertyTaxExemption'
import PolicyAvistaCommercialEnergyEfficiencyIncentivesProgram from './components/instances/PolicyAvistaCommercialEnergyEfficiencyIncentivesProgram'
import PolicyGeorgiaAdvancedSolarInitiative from './components/instances/PolicyGeorgiaAdvancedSolarInitiative'
import PolicySaintPaulPaceProgram from './components/instances/PolicySaintPaulPaceProgram'
import PolicyAlbuquerqueSolarEasementandRightsLaws from './components/instances/PolicyAlbuquerqueSolarEasementandRightsLaws'
import About from './components/About'
import { Switch, Route} from "react-router-dom";
import './App.css';

export default function App() {
  return (
        <div className="App">
            {/* Display NavBar */}
            <NavBar />

            {/* Define Routes */}
            <Switch>
                <Route path="/About">
                    <About />
                </Route>
                <Route path="/Organizations/SouthernPowerCo">
                    <OrganizationSouthernPowerCo />
                </Route>
                <Route path="/Organizations/SouthwesternPublicServiceCo">
                    <OrganizationSouthwesternPublicServiceCo />
                </Route>
                <Route path="/Organizations/ColumbiaBasinHydropower">
                    <OrganizationColumbiaBasinHydropower />
                </Route>
                <Route path="/Organizations">
                    <Organizations />
                </Route>
                <Route path="/PowerPlants/MachoSprings">
                    <PowerPlantMachoSprings />
                </Route>
                <Route path="/PowerPlants/Maddox">
                    <PowerPlantMaddox />
                </Route>
                <Route path="/PowerPlants/MainCanalHeadworks">
                    <PowerPlantMainCanalHeadworks />
                </Route>
                <Route path="/PowerPlants">
                    <PowerPlants />
                </Route>
                <Route path="/Cities/Albuquerque">
                    <CityAlbuquerque />
                </Route>
                <Route path="/Cities/Atlanta">
                    <CityAtlanta />
                </Route>
                <Route path="/Cities/Ephrata">
                    <CityEphrata />
                </Route>
                <Route path="/Cities/Minneapolis">
                    <CityMinneapolis />
                </Route>
                <Route path="/Cities">
                    <Cities />
                </Route>
                <Route path="/Policies/solarandwindenergydevicefranchisetaxdeduction">
                    <PolicySolarandWindEnergyDeviceFranchiseTaxDeduction />
                </Route>
                <Route path="/Policies/solarandwindenergybusinessfranchisetaxexemption">
                    <PolicySolarandWindEnergyBusinessFranchiseTaxExemption />
                </Route>
                <Route path="/Policies/renewableenergysystemspropertytaxexemption">
                    <PolicyRenewableEnergySystemsPropertyTaxExemption />
                </Route>
                <Route path="/Policies/avistacommercialenergyefficiencyincentivesprogram">
                    <PolicyAvistaCommercialEnergyEfficiencyIncentivesProgram />
                </Route>
                <Route path="/Policies/georgiaadvancedsolarinitiative">
                    <PolicyGeorgiaAdvancedSolarInitiative />
                </Route>
                <Route path="/Policies/saintpaulpaceprogram">
                    <PolicySaintPaulPaceProgram />
                </Route>
                <Route path="/Policies/albuquerquesolareasementandrightslaws">
                    <PolicyAlbuquerqueSolarEasementandRightsLaws />
                </Route>
                <Route path="/Policies">
                    <Policies />
                </Route>
                <Route path="/">
                    <Home />
                </Route>
            </Switch>

      </div>
    );
}

