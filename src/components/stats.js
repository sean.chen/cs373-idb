import React from 'react';
import Table from 'react-bootstrap/Table'
import {GITLAB_TKN} from '../constants'

class GitlabStats extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        error: null,
        isLoaded: false,
        commits: 0,
        issues: 0,
        unit_tests: 0
      };
    }
  
    componentDidMount() {
      //Need to move token to a constants file
      var bearer = "Bearer " + GITLAB_TKN;

      //API file?
      //Grab Commits
      fetch("https://gitlab.com/api/v4/projects/21327660/repository/commits?all=true&per_page=100", {method: 'GET', headers: new Headers({'Authorization': bearer})})
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              isLoaded: true,
              commits: result.length
            });
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          (error) => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        )

      //API file?
      //Grab Issues
      fetch("https://gitlab.com/api/v4/projects/21327660/issues?scope=all&per_page=100", {method: 'GET', headers: new Headers({'Authorization': bearer})})
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            issues: result.length
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
    }
  
    render() {
      const { error, isLoaded, commits, issues, unit_tests } = this.state;
      if (error) {
        return <div>Error: {error.message}</div>;
      } else if (!isLoaded) {
        return <div>Loading...</div>;
      } else {
        return (
        <div>
            <Table variant='dark'>
              <thead>
                    <tr>
                    <th>No. Commits</th>
                    <th>No. Issues</th>
                    <th>No. Unit Tests</th>
                    </tr>
              </thead>
              <tbody>
                <tr>
                    <td>{commits}</td>
                    <td>{issues}</td>
                    <td>{unit_tests}</td>
                </tr>
              </tbody>
          </Table>
        </div>
        );
      }
    }
  }

  class MemberStats extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        error: null,
        isLoaded: false,
        commits: 0,
        issues: 0,
        unit_tests: 0
      };
    }
  
    componentDidMount() {
      //Need to move token to a constants file
      var bearer = "Bearer " + GITLAB_TKN;

      //API file?
      //Grab Commits
      fetch("https://gitlab.com/api/v4/projects/21327660/repository/commits?all=true&per_page=100", {method: 'GET', headers: new Headers({'Authorization': bearer})})
        .then(res => res.json())
        .then(
          (result) => {
            var commit_count = 0;
            for(var i = 0; i < result.length; i++){
              var email = String(result[i]["author_email"]).trim();
              var user_emails = String(this.props.email);
            
              if(user_emails.includes(email)){
                commit_count += 1;
              }
              
            }
            this.setState({
              isLoaded: true,
              commits: commit_count
            });
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          (error) => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        )

      //API file?
      //Grab Issues
      fetch("https://gitlab.com/api/v4/projects/21327660/issues?scope=all&per_page=100", {method: 'GET', headers: new Headers({'Authorization': bearer})})
      .then(res => res.json())
      .then(
        (result) => {
          var issue_count = 0;
          for(var i = 0; i < result.length; i++){
            var name = result[i]["author"]["username"]
            if(this.props.username === name){
              issue_count += 1
            }
          }
          this.setState({
            isLoaded: true,
            issues: issue_count
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
    }
  
    render() {
      const { error, isLoaded, commits, issues, unit_tests } = this.state;
      if (error) {
        return <div>Error: {error.message}</div>;
      } else if (!isLoaded) {
        return <div>Loading...</div>;
      } else {
        return (
        <div>
            <Table>
              <thead>
                    <tr>
                    <th>No. Commits</th>
                    <th>No. Issues</th>
                    <th>No. Unit Tests</th>
                    </tr>
              </thead>
              <tbody>
                <tr>
                    <td>{commits}</td>
                    <td>{issues}</td>
                    <td>{unit_tests}</td>
                </tr>
              </tbody>
          </Table>
        </div>
        );
      }
    }
  }
  export {GitlabStats, MemberStats};