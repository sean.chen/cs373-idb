import React from 'react';
import '../App.css';
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function Cities() {
    return (
        <div className="App">
            <header className="App-header">
                <h1>Cities Overview Page</h1>
                <Table variant="dark">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>State</th>
                            <th>Population</th>
                            <th>Longitude</th>
                            <th>Latitude</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><Link to="/Cities/Albuquerque">Albuquerque</Link></td>
                            <td>New Mexico</td>
                            <td>560,504</td>
                            <td>34.5199° N</td>
                            <td>105.8701° W</td>
                        </tr>
                        <tr>
                            <td><Link to="/Cities/Atlanta">Atlanta</Link></td>
                            <td>Georgia</td>
                            <td>506,804</td>
                            <td>33.7490° N</td>
                            <td>84.3880° W</td>
                        </tr>
                        <tr>
                        <td><Link to="/Cities/Ephrata">Ephrata</Link></td>
                            <td>Washington</td>
                            <td>8,050</td>
                            <td>47.3176° N</td>
                            <td>119.5536° W</td>
                        </tr>
                        <tr>
                            <td><Link to="/Cities/Minneapolis">Minneapolis</Link></td>
                            <td>Minnesota</td>
                            <td>429,605</td>
                            <td>44.9778° N</td>
                            <td>93.2650° W</td>
                        </tr>
                    </tbody>
                </Table>
            </header>
        </div>
    );
}
