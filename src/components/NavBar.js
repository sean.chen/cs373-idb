import React from 'react';
import { Nav, Navbar } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import '../App.css';


function NavBar() {
  return (
    <div>
      <Navbar bg="primary" variant="dark" expand="lg">
        <Navbar.Brand href="/">PowerNet</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <LinkContainer to="/Organizations">
              <Nav.Link>Organizations</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/PowerPlants">
              <Nav.Link>Power Plants</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/Cities">
              <Nav.Link>Cities</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/Policies">
              <Nav.Link>Policies</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/About">
              <Nav.Link>About</Nav.Link>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
        <Navbar.Text>
          <a href="https://documenter.getpostman.com/view/12849192/TVRdAXQX">API Documentation</a>
        </Navbar.Text>  
      </Navbar>
    </div>
  );
}

export default NavBar;