import React from 'react';
import '../../App.css';
import { Table } from 'react-bootstrap'
import { Link } from "react-router-dom";

export default function PolicyGeorgiaAdvancedSolarInitiative() {
    return (
        <div className="App">
            <header className="App-header">
              <h1>Georgia Power - Advanced Solar Initiative</h1>
              <td>Year: 2015</td>
              <td>Type: Performance Based Incentive</td>
              <td>Affected Cities: <Link to="/Cities/Atlanta">Atlanta</Link></td>
              <td><a href="www.georgiapower.com/about-energy/energy-sources/solar/asi/advanced-solar-initiative.cshtml">Link</a></td>
              <h1>Description</h1>
              <p><b>Note: According to Georgia Power's website, the Advanced Solar Initiative's final program guidelines are due to be published on June 25th and the bidding period for is expected to open on July 10, 2015. The projected timeline for the Advanced Solar Initiative is <a href="http://georgiapower.com/about-energy/energy-sources/solar/asi/small-medium.cshtml">here.</a></b></p><p>Georgia Power's Advanced Solar Initiative consists of a Utility Scale RFP program and a Distributed Generation program and is overseen by an independent evaluator,<a href="https://gpcasidg.accionpower.com/_solar_1501/accionhome.asp"> the Accion Group.</a></p><p>The Utility Scale RFP program consists of a competitive bidding process for 50 MWs of capacity. Systems between 1 and 3 MW have been allotted 40 MWs of capacity, while 10 MW has been alotted for systems between 500 kilowatts and 1 MW. </p><p>The Distributed Generation program has 50 MW available with preference and fixed pricing (as yet unannounced). A total of 40 MW is available for non-customer sited systems between 100 kW and 500 kW and also for customer-sited systems between 100kW and 1 MW. Another 10 MW is available for projects less than 100 kW. Customer-sited projects must have a peak generating capacity that is <span>less than 125% of the customer's annual peak demand.</span></p><p>Special provision for both programs are available for systems sited on Greenfields.</p>              
              </header>
        </div>
    );
}