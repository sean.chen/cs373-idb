import React from 'react';
import '../../App.css';
import { Table } from 'react-bootstrap'

import {
  Link
} from "react-router-dom";

export default function OrganizationColumbiaBasinHydropower() {
    return (
        <div className="App">
            <header className="App-header">
                <h1>Columbia Basin Hydropower</h1>
                  <img className="logobig" src="https://powernet-resources-dev.s3.us-east-2.amazonaws.com/columbiabasinlogo.png"/>
                  <td data-title="CB Hydropower provides administration, operations, and maintenance for hydroelectric facilities owned by the three Columbia Basin Irrigation Districts.  The generation we produce benefits the local economy and the farmers of the Columbia Basin.">CB Hydropower provides administration,...</td>
                  <td><a href="http://www.cbhydropower.org/">http://www.cbhydropower.org/</a></td>
                  <td><Link to="/Cities/Ephrata">Ephrata, WA</Link></td>
                  <h1> Owned Power Plants </h1>
                  <Table variant="dark">
                      <thead>
                        <tr>
                          <th>Name</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><Link to="/PowerPlants/MainCanalHeadworks">Main Canal Headworks</Link></td>
                        </tr>
                      </tbody>
                </Table>
            </header>
        </div>
    );
}