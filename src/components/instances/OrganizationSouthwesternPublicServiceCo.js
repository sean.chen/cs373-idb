import React from 'react';
import '../../App.css';
import { Table } from 'react-bootstrap'

import {
  Link
} from "react-router-dom";

export default function OrganizationSouthwesternPublicServiceCo() {
    return (
        <div className="App">
            <header className="App-header">
                <h1>Southwestern Public Service Co</h1>
                  <img className="logobig" src="https://powernet-resources-dev.s3.us-east-2.amazonaws.com/southwesternlogo.jpeg"/>
                  <td> Southwestern Public Service Company generates and transmits electric energy. The Company offers energy audit, home lighting, renewable energy programs, energy saving tips, electricity outage preparation, safety, solar, biomass, wind, and hydro power.</td>
                  <td><a href="https://www.xcelenergy.com/">https://www.xcelenergy.com/</a></td>
                  <td><Link to="/Cities/Minneapolis">Minneapolis, MN</Link></td>
                  <h1> Owned Power Plants </h1>
                    <Table variant="dark">
                        <thead>
                            <tr>
                            <th>Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><Link to="/PowerPlants/Maddox">Maddox</Link></td>
                            </tr>
                        </tbody>
                  </Table>
            </header>
        </div>
    );
}