import React from 'react';
import '../../App.css';
import { Table } from 'react-bootstrap'
import { Link } from "react-router-dom";

export default function PolicyAlbuquerqueSolarEasementandRightsLaws() {
    return (
        <div className="App">
            <header className="App-header">
              <h1>City of Albuquerque - Solar Easements and Rights Laws</h1>
              <td>Year: 2014</td>
              <td>Type: Solar/Wind Access Policy</td>
              <td>Affected Cities: <Link to="/Cities/Albuquerque">Albuquerque</Link></td>
              <td>Affected Power Plants: <Link to="/PowerPlants/MachoSprings">Macho Springs</Link>, <Link to="/PowerPlants/Maddox">Maddox</Link></td>
              <h1>Description</h1>
              <p>Albuquerque replaced its Zoning Code, Subdivision Ordinance, and Sector Development plans in 2018 with an Integrated Development Ordinance (IDO). The IDO states that the city &#34;may not approve any subdivision application for property on which there are any <span>deed restrictions, covenants, or binding agreements prohibiting solar collectors from being </span><span>installed on buildings or erected on the lots or parcels within the application.&#34;</span></p>&#10;<p>&#10;&#9;The State of New Mexico has a separate <a href="http://www.dsireusa.org/incentives/incentive.cfm?Incentive_Code=NM02R&amp;re=0&amp;ee=0">solar rights law</a>.</p>&#10;<p>&#10;&#9;<strong>Permit for Solar Rights</strong></p>&#10;<p>&#10;&#9;A solar right is granted through a permitting process (§ 14-11-6 et seq.). The spatial and temporal limits of one’s solar right are defined by the permit for solar rights issued for solar collectors. The permit for solar rights can grant solar access such that the time periods include between 9am and 3pm at the winter solstice and between 9am and 5pm on the summer solstice. The application fee is $50.</p>&#10;<p>&#10;&#9;A permit for solar rights belongs to the property upon which the solar collector is situated. A solar right is enforceable as an “easement appurtenant” against any person whose obstruction first infringed or was planned to infringe on such right after that person had knowledge or notice of the solar right.</p>&#10;<p>&#10;&#9;Neither a solar right nor a permit for solar rights is transferable separately from the property upon which the related solar collector is located (unless the right is acquired for a public purpose by an entity which has power of eminent domain). However, the owner of the property burdened by the permit may buy the buy the permit from its owner and terminate or diminish the solar right.</p>&#10;<p><br/></p>
            </header>
        </div>
    );
}
