// Albuquerque
import React from 'react';
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import ReactPlayer from "react-player"
import '../../App.css';

export default function CityAlbuquerque() {
    return (
        <div className="App">
            <header className="App-header">
                <h1>Albuquerque</h1>
                <ReactPlayer url="https://www.youtube.com/watch?v=iB4XbL_PmzQ&ab_channel=Expedia"/>
                <Table variant="dark">
                    <tbody>
                        <tr>
                            <td>State</td>
                            <td>New Mexico</td>
                        </tr>
                        <tr>
                            <td>Population</td>
                            <td>560,504</td>
                        </tr>
                        <tr>
                            <td>Longitude</td>
                            <td>34.5199° N</td>
                        </tr>
                        <tr>
                            <td>Latitude</td>
                            <td>105.8701° W</td>
                        </tr>
                    </tbody>
                </Table>
                <h5>State-Level Electric Power Industry Emissions</h5>
                <h5>(in Metric Tons)</h5>
                <Table variant="dark">
                    <tbody>
                        <tr>
                            <th>CO2</th>
                            <th>18,442,013</th>
                        </tr>
                        <tr>
                            <th>SO2</th>
                            <th>3,181</th>
                        </tr>
                        <tr>
                            <th>NOx</th>
                            <th>16,168</th>
                        </tr>
                    </tbody>
                </Table>
                <h5>State-Level Policies</h5>
                <Table variant="dark">
                    <tbody>
                        <td><Link to="/Policies/albuquerquesolareasementandrightslaws">City of Albuquerque - Solar Easements and Rights Laws</Link></td>
                    </tbody>
                </Table>
            </header>
        </div>
    );
}