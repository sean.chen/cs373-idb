import React from 'react';
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import ephratacity from "../../ephratacity.png"
import '../../App.css';

export default function CityEphrata() {
    return (
        <div className="App">
            <header className="App-header">
                <h1>Ephrata</h1>
                <img src={ephratacity} alt="image not found" />
                <Table variant="dark">
                    <tbody>
                        <tr>
                            <td>State</td>
                            <td>Washington</td>
                        </tr>
                        <tr>
                            <td>Population</td>
                            <td>8,050</td>
                        </tr>
                        <tr>
                            <td>Longitude</td>
                            <td>47.3176° N</td>
                        </tr>
                        <tr>
                            <td>Latitude</td>
                            <td>119.5536° W</td>
                        </tr>
                    </tbody>
                </Table>
                <h5>State-Level Electric Power Industry Emissions</h5>
                <h5>(in Metric Tons)</h5>
                <Table variant="dark">
                    <tbody>
                        <tr>
                            <th>CO2</th>
                            <th>10,660,849</th>
                        </tr>
                        <tr>
                            <th>SO2</th>
                            <th>10,844</th>
                        </tr>
                        <tr>
                            <th>NOx</th>
                            <th>14,133</th>
                        </tr>
                    </tbody>
                </Table>
                <h5>State-Level Policies</h5>
                <Table variant="dark">
                    <tbody>
                        <td><Link to="/Policies/avistacommercialenergyefficiencyincentivesprogram">Avista Utilities (Electric) - Commercial Energy Efficiency Incentives Program</Link></td>
                    </tbody>
                </Table>
                <h5>Organizations Based In This City</h5>
                <Table variant="dark">
                    <tbody>
                        <td><Link to="/PowerPlants/MainCanalHeadworks">Columbia Basin Hydropower</Link></td>
                    </tbody>
                </Table>
            </header>
        </div>
    );
}