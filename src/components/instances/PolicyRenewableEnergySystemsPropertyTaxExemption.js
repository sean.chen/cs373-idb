import React from 'react';
import '../../App.css';
import { Table } from 'react-bootstrap'
import { Link } from "react-router-dom";

export default function PolicyRenewableEnergySystemsPropertyTaxExemption() {
    return (
        <div className="App">
            <header className="App-header">
              <h1>Renewable Energy Systems Property Tax Exemption</h1>
              <td>Year: 2000</td>
              <td>Type: Property Tax Incentive</td>
              <td><a href="https://comptroller.texas.gov/taxes/property-tax/exemptions/">Link</a></td>
              <h1>Description</h1>
                <p>&#10;&#9;<span>The Texas property tax code allows an exemption of the amount of 100% of the appraised property value increase arising from the installation or construction of a solar or wind-powered energy device that is primarily for the production and distribution of thermal, mechanical, or electrical energy for on-site use and devices used to store that energy. </span></p>&#10;<p>&#10;&#9;<span>Solar energy devices installed or constructed on or after January 1, 2014, used for a commercial purpose are subject to the cost method of appraisal, and the depreciated value must be calculated using a useful life of 10 years or less (<a href="http://legiscan.com/TX/text/HB2500/id/851632/Texas-2013-HB2500-Enrolled.html">H.B. 2500</a>).</span></p>&#10;<p>&#10;&#9;<b><span>Eligible Technologies</span></b></p>&#10;<p>&#10;&#9;<span>&#34;Solar&#34; is broadly defined and includes a range of biomass technologies. &#34;Solar energy device&#34; means an apparatus designed or adapted to convert the radiant energy from the sun, including energy imparted to plants through photosynthesis employing the bio-conversion processes of anaerobic digestion, gasification, pyrolysis, or fermentation, but not including direct combustion, into thermal, mechanical, or electrical energy; to store the converted energy, either in the form to which originally converted or another form; or to distribute radiant solar energy or the energy to which the radiant solar energy is converted.</span></p>&#10;<p>&#10;&#9;<span>&#34;Wind-powered energy device&#34; means an apparatus designed or adapted to convert the energy available in the wind into thermal, mechanical, or electrical energy; to store the converted energy, either in the form to which originally converted or another form; or to distribute the converted energy.</span></p>&#10;<p>&#10;&#9;<b><span>Process</span></b></p>&#10;<p>&#10;&#9;<span>Those wishing to claim this exemption must fill out <a href="http://www.window.state.tx.us/taxinfo/taxforms/50-123.pdf" title="Form 50-123">Form 50-123</a>, “Exemption Application for Solar or Wind-Powered Energy Devices.”</span></p><p></p><p>More information can be found in the <a href="https://comptroller.texas.gov/taxes/property-tax/docs/96-1569.pdf">Solar and Wind-Powered Energy Device Exemption and Appraisal Guidelines.</a></p><p></p>            </header>
        </div>
    );
}