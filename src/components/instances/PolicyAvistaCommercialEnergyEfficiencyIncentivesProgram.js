import React from 'react';
import '../../App.css';
import { Table } from 'react-bootstrap'
import { Link } from "react-router-dom";

export default function PolicyAvistaCommercialEnergyEfficiencyIncentivesProgram() {
    return (
        <div className="App">
            <header className="App-header">
              <h1>Avista Utilities (Electric) - Commercial Energy Efficiency Incentives Program</h1>
              <td>Year: 2016</td>
              <td>Type: Rebate Program</td>
              <td>Affected Cities: <Link to="/Cities/Ephrata">Ephrata</Link></td>
              <td>Affected Power Plants: <Link to="/PowerPlants/MainCanalHeadworks">Main Canal Headworks</Link></td>
              <td><a href="https://www.myavista.com/energy-savings/tools-for-your-business/rebates-washington">Link</a></td>
              <h1>Description</h1>
              <td>You can receive incentives for improving the energy efficiency of your business. Many projects not only save electricity and natural gas, but other resources, as well. Be sure you meet program eligibility and guidelines for the rebate you are interested in.</td>
              </header>
        </div>
    );
}