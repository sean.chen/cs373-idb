import React from 'react';
import '../../App.css';
import { Table } from 'react-bootstrap'
import { Link } from "react-router-dom";

export default function PolicySolarandWindEnergyDeviceFranchiseTaxDeduction() {
    return (
        <div className="App">
            <header className="App-header">
              <h1>Solar and Wind Energy Device Franchise Tax Deduction</h1>
              <td>Year: 2000</td>
              <td>Type: Corporate Tax Deduction</td>
              <td><a href="http://www.seco.cpa.state.tx.us/re/incentives-taxcode-statutes.php">Link</a></td>
              <h1>Description</h1>
              <p>&#10;&#9;<span>Texas allows a corporation to deduct the cost of a solar energy device from the franchise tax in one of two ways: <br/></span></p>&#10;<ul>&#10;&#9;<li>&#10;&#9;&#9;<span>The total cost of the system may be deducted from the company's taxable capital or, </span></li>&#10;&#9;<li>&#10;&#9;&#9;<span>10% of the system's cost may be deducted from the company's income.  </span></li>&#10;</ul>&#10;<p>&#10;&#9;<span>Both taxable capital and a company's income are taxed under the franchise tax, which is Texas's equivalent to a corporate tax. </span></p>&#10;<p>&#10;&#9;<span>For the purposes of this deduction, a solar energy device means &#34;a system or series of mechanisms designed primarily to provide heating or cooling or to produce electrical or mechanical power by collecting and transferring solar-generated energy. The term includes a mechanical or chemical device that has the ability to store solar-generated energy for use in heating or cooling or in the production of power.&#34; Under this definition wind energy is also included as an eligible technology.</span></p>&#10;<p>&#10;&#9;<span>Texas also offers a<span class="apple-converted-space"> </span><a href="http://programs.dsireusa.org/system/program/detail/82" target="_blank" title="franchise tax exemption">franchise tax exemption</a><span class="apple-converted-space"> </span>for manufacturers, seller, or installers of solar energy systems which also includes wind energy as an eligible technology.</span></p>
            </header>
        </div>
    );
}