import React from 'react';
import '../../App.css';
import { Table } from 'react-bootstrap'

import {
  Link
} from "react-router-dom";

export default function PowerPlantMaddox() {
    return (
        <div className="App">
            <header className="App-header">
                <h1>Maddox</h1>
                  <img className="mapbig" src="https://powernet-resources-dev.s3.us-east-2.amazonaws.com/Maddox.png"/>
                  <td>Owned by <Link to="/Organizations/SouthwesternPublicServiceCo">Southwestern Public Service Co</Link></td>
                  <h1> Owned Power Plants </h1>
                <h1> Stats </h1>
                <Table variant="dark">
                    <thead>
                      <tr>
                        <th>Capacity (MW)</th>
                        <th>Latitude</th>
                        <th>Longitude</th>
                        <th>Fuel</th>
                        <th>Comissioning Year</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>212</td>
                        <td>32.7142</td>
                        <td>-103.3015</td>
                        <td>Gas</td>
                        <td>1970</td>
                      </tr>
                    </tbody>
                </Table>
                <h1> Potentially Applicable Policies </h1>
                <Table variant="dark">
                    <thead>
                        <tr>
                        <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><Link to="/Policies/albuquerquesolareasementandrightslaws">City of Albuquerque - Solar Easements and Rights Laws</Link></td>
                        </tr>
                    </tbody>
                </Table>
            </header>
        </div>
    );
}