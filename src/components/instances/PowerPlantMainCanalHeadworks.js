import React from 'react';
import '../../App.css';
import { Table } from 'react-bootstrap'

import {
  Link
} from "react-router-dom";

export default function PowerPlantMainCanalHeadworks() {
    return (
        <div className="App">
            <header className="App-header">
                <h1>Main Canal Headworks</h1>
                  <img className="mapbig" src="https://powernet-resources-dev.s3.us-east-2.amazonaws.com/MainCanalHeadworks.png"/>
                  <td>Owned by <Link to="/Organizations/ColumbiaBasinHydropower">Columbia Basin Hydropower</Link></td>
                  <h1> Stats </h1>
                  <Table variant="dark">
                      <thead>
                        <tr>
                          <th>Capacity (MW)</th>
                          <th>Latitude</th>
                          <th>Longitude</th>
                          <th>Fuel</th>
                          <th>Comissioning Year</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>26.8</td>
                          <td>47.6164</td>
                          <td>-119.2992</td>
                          <td>Hydro</td>
                          <td>1987</td>
                        </tr>
                      </tbody>
                </Table>
                <h1> Potentially Applicable Policies </h1>
                <Table variant="dark">
                    <thead>
                        <tr>
                        <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><Link to="/Policies/avistacommercialenergyefficiencyincentivesprogram">Avista Utilities (Electric) - Commercial Energy Efficiency Incentives Program</Link></td>
                        </tr>
                    </tbody>
                </Table>
            </header>
        </div>
    );
}