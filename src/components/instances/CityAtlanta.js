import React from 'react';
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import ReactPlayer from "react-player"
import '../../App.css';

export default function CityAtlanta() {
    return (
        <div className="App">
            <header className="App-header">
                <h1>Atlanta</h1>
                <ReactPlayer url="https://www.youtube.com/watch?v=iB4XbL_PmzQ&ab_channel=Expedia"/>
                <Table variant="dark">
                    <tbody>
                        <tr>
                            <td>State</td>
                            <td>Georgia</td>
                        </tr>
                        <tr>
                            <td>Population</td>
                            <td>506,804</td>
                        </tr>
                        <tr>
                            <td>Longitude</td>
                            <td>33.7490° N</td>
                        </tr>
                        <tr>
                            <td>Latitude</td>
                            <td>84.3880° W</td>
                        </tr>
                    </tbody>
                </Table>
                <h5>State-Level Electric Power Industry Emissions</h5>
                <h5>(in Metric Tons)</h5>
                <Table variant="dark">
                    <tbody>
                        <tr>
                            <th>CO2</th>
                            <th>54,803,366</th>
                        </tr>
                        <tr>
                            <th>SO2</th>
                            <th>48,010</th>
                        </tr>
                        <tr>
                            <th>NOx</th>
                            <th>42,989</th>
                        </tr>
                    </tbody>
                </Table>
                <h5>State-Level Policies</h5>
                <Table variant="dark">
                    <tbody>
                        <td><Link to="/Policies/georgiaadvancedsolarinitiative">Georgia Power - Advanced Solar Initiative</Link></td>
                    </tbody>
                </Table>
                
                <h5>Organizations Based In This City</h5>
                <Table variant="dark">
                    <tbody>
                        <td><Link to="/PowerPlants/MachoSprings">Southern Power Co</Link></td>
                    </tbody>
                </Table>
            </header>
        </div>
    );
}