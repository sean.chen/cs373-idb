import React from 'react';
import { Table } from "react-bootstrap";
import ReactPlayer from "react-player"
import { Link } from "react-router-dom";
import '../../App.css';

export default function CityMinneapolis() {
    return (
        <div className="App">
            <header className="App-header">
                <h1>Minneapolis</h1>
                <ReactPlayer url="https://www.youtube.com/watch?v=CWzZ-Y2D9tw&ab_channel=TampaAerialMedia"/>
                <Table variant="dark">
                    <tbody>
                        <tr>
                            <td>State</td>
                            <td>Minnesota</td>
                        </tr>
                        <tr>
                            <td>Population</td>
                            <td>429,605</td>
                        </tr>
                        <tr>
                            <td>Longitude</td>
                            <td>44.9778° N</td>
                        </tr>
                        <tr>
                            <td>Latitude</td>
                            <td>93.2650° W</td>
                        </tr>
                    </tbody>
                </Table>
                <h5>State-Level Electric Power Industry Emissions</h5>
                <h5>(in Metric Tons)</h5>
                <Table variant="dark">
                    <tbody>
                        <tr>
                            <th>CO2</th>
                            <th>29,805,125</th>
                        </tr>
                        <tr>
                            <th>SO2</th>
                            <th>23,994</th>
                        </tr>
                        <tr>
                            <th>NOx</th>
                            <th>26,748</th>
                        </tr>
                    </tbody>
                </Table>
                <h5>State-Level Policies</h5>
                <Table variant="dark">
                    <tbody>
                        <td><Link to="/Policies/saintpaulpaceprogram">Saint Paul Port Authority PACE Program</Link></td>
                    </tbody>
                </Table>
                <h5>Organizations Based In This City</h5>
                <Table variant="dark">
                    <tbody>
                        <td><Link to="/PowerPlants/Maddox">Southwestern Public Service Co</Link></td>
                    </tbody>
                </Table>
            </header>
        </div>
    );
}