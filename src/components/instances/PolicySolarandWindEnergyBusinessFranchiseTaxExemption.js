import React from 'react';
import '../../App.css';
import { Table } from 'react-bootstrap'
import { Link } from "react-router-dom";

export default function PolicySolarandWindEnergyBusinessFranchiseTaxExemption() {
    return (
        <div className="App">
            <header className="App-header">
              <h1>Solar and Wind Energy Business Franchise Tax Exemption</h1>
              <td>Year: 2000</td>
              <td>Type: Industry Recruitment/Support</td>
              <td><a href="http://www.seco.cpa.state.tx.us/re/incentives-taxcode-statutes.php">Link</a></td>
              <h1>Description</h1>
              <p>&#10;&#9;Companies in Texas engaged solely in the business of manufacturing, selling, or installing solar or wind energy devices are exempt from the franchise tax. The franchise tax is Texas’s equivalent to a corporate tax. There is no ceiling on this exemption, so it can be a substantial incentive for solar and wind businesses.<br/>&#10;&#9;<br/>&#10;&#9;For the purposes of this exemption, a solar energy device means &#34;a system or series of mechanisms designed primarily to provide heating or cooling or to produce electrical or mechanical power by collecting and transferring solar-generated energy. The term includes a mechanical or chemical device that has the ability to store solar-generated energy for use in heating or cooling or in the production of power.&#34; Under this definition wind energy is also listed as an eligible technology.<br/>&#10;&#9;<br/>&#10;&#9;Texas also offers a <a href="http://programs.dsireusa.org/system/program/detail/81" class="summarylink" target="_blank" title="franchise tax deduction">franchise tax deduction</a> for solar energy devices which also includes wind energy as an eligible technology.</p>
            </header>
        </div>
    );
}