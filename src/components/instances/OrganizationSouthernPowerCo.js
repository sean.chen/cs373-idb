import React from 'react';
import '../../App.css';
import { Table } from 'react-bootstrap'

import {
  Link
} from "react-router-dom";

export default function OrganizationSouthernPowerCo() {
    return (
        <div className="App">
            <header className="App-header">
                <h1>Southern Power Co</h1>
              <img className="logobig" src="https://www.southernpowercompany.com/content/dam/southernpower/images/logos/southern_power_h_cmyk"/>
              <td data-title="Southern Power builds the future of energy by investing in clean energy solutions for the customers we serve. A subsidiary of Southern Company, we are a leading U.S. wholesale energy provider meeting the electricity needs of municipalities, electric cooperatives, investor-owned utilities, and commercial and industrial customers.">Southern Power builds the...</td>
              <td><a href="https://www.southernpowercompany.com/">https://www.southernpowercompany.com/</a></td>
              <td><Link to="/Cities/Atlanta">Atlanta, GA</Link></td>

              <h1> Owned Power Plants </h1>
              <Table variant="dark">
                  <thead>
                    <tr>
                      <th>Name</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><Link to="/PowerPlants/MachoSprings">Macho Springs</Link></td>
                    </tr>
                  </tbody>
            </Table>
            </header>
        </div>
    );
}