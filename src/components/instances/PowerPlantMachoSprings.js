import React from 'react';
import '../../App.css';
import { Table } from 'react-bootstrap'

import {
  Link
} from "react-router-dom";

export default function PowerPlantMachoSprings() {
    return (
        <div className="App">
            <header className="App-header">
                <h1>Macho Springs</h1>
                  <img className="mapbig" src="https://powernet-resources-dev.s3.us-east-2.amazonaws.com/MachoSprings.png"/>
                  <td>Owned by <Link to="/Organizations/SouthernPowerCo">Southern Power Co</Link></td>
                  <h1> Owned Power Plants </h1>
                    <h1> Stats </h1>
                    <Table variant="dark">
                        <thead>
                          <tr>
                            <th>Capacity (MW)</th>
                            <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Fuel</th>
                            <th>Comissioning Year</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>52.2</td>
                            <td>32.57</td>
                            <td>-107.48</td>
                            <td>Solar</td>
                            <td>2014</td>
                          </tr>
                        </tbody>
                  </Table>
                  <h1> Potentially Applicable Policies </h1>
                    <Table variant="dark">
                        <thead>
                            <tr>
                            <th>Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><Link to="/Policies/albuquerquesolareasementandrightslaws">City of Albuquerque - Solar Easements and Rights Laws</Link></td>
                            </tr>
                        </tbody>
                  </Table>
            </header>
        </div>
    );
}