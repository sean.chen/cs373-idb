import React from 'react';
import '../App.css';
import { Table } from 'react-bootstrap'

import {
  Link
} from "react-router-dom";

export default function Organizations() {
    return (
        <div className="App">
            <header className="App-header">
                <h1>Organizations Overview Page</h1>
                    <Table variant="dark">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Logo</th>
                          <th>Description</th>
                          <th>Website</th>
                          <th>Headquarters</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><Link to="/Organizations/SouthernPowerCo">Southern Power Co</Link></td>
                          <img className="logo" src="https://www.southernpowercompany.com/content/dam/southernpower/images/logos/southern_power_h_cmyk"/>
                          <td data-title="Southern Power builds the future of energy by investing in clean energy solutions for the customers we serve. A subsidiary of Southern Company, we are a leading U.S. wholesale energy provider meeting the electricity needs of municipalities, electric cooperatives, investor-owned utilities, and commercial and industrial customers.">Southern Power builds the...</td>
                          <td><a href="https://www.southernpowercompany.com/">https://www.southernpowercompany.com/</a></td>
                          <td><Link to="/Cities/Atlanta">Atlanta, GA</Link></td>
                        </tr>
                        <tr>
                          <td><Link to="/Organizations/SouthwesternPublicServiceCo">Southwestern Public Service Co</Link></td>
                          <img className="logo" src="https://powernet-resources-dev.s3.us-east-2.amazonaws.com/southwesternlogo.jpeg"/>
                          <td data-title="Southwestern Public Service Company generates and transmits electric energy. The Company offers energy audit, home lighting, renewable energy programs, energy saving tips, electricity outage preparation, safety, solar, biomass, wind, and hydro power.">Southwestern Public Service Company generates...</td>
                          <td><a href="https://www.xcelenergy.com/">https://www.xcelenergy.com/</a></td>
                          <td><Link to="/Cities/Minneapolis">Minneapolis, MN</Link></td>
                        </tr>
                        <tr>
                          <td><Link to="/Organizations/ColumbiaBasinHydropower">Columbia Basin Hydropower</Link></td>
                          <img className="logo" src="https://powernet-resources-dev.s3.us-east-2.amazonaws.com/columbiabasinlogo.png"/>
                          <td data-title="CB Hydropower provides administration, operations, and maintenance for hydroelectric facilities owned by the three Columbia Basin Irrigation Districts.  The generation we produce benefits the local economy and the farmers of the Columbia Basin.">CB Hydropower provides administration,...</td>
                          <td><a href="http://www.cbhydropower.org/">http://www.cbhydropower.org/</a></td>
                          <td><Link to="/Cities/Ephrata">Ephrata, WA</Link></td>
                        </tr>
                      </tbody>
                    </Table>
            </header>
        </div>
    );
}
