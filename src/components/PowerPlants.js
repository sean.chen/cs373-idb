import React from 'react';
import '../App.css';
import { Table } from 'react-bootstrap'

import {
  Link
} from "react-router-dom";

export default function PowerPlants() {
    return (
        <div className="App">
            <header className="App-header">
                <h1>Power Plants Overview Page</h1>
                <Table variant="dark">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Capacity (MW)</th>
                      <th>Latitude</th>
                      <th>Longitude</th>
                      <th>Fuel</th>
                      <th>Comissioning Year</th>
                      <th>Owner</th>
                      <th>Image</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><Link to="/PowerPlants/MachoSprings">Macho Springs</Link></td>
                      <td>52.2</td>
                      <td>32.57</td>
                      <td>-107.48</td>
                      <td>Solar</td>
                      <td>2014</td>
                      <td><Link to="/Organizations/SouthernPowerCo">Southern Power Co</Link></td>
                      <img className="map" src="https://powernet-resources-dev.s3.us-east-2.amazonaws.com/MachoSprings.png"/>
                    </tr>
                    <tr>
                      <td><Link to="/PowerPlants/Maddox">Maddox</Link></td>
                      <td>212</td>
                      <td>32.7142</td>
                      <td>-103.3015</td>
                      <td>Gas</td>
                      <td>1970</td>
                      <td><Link to="/Organizations/SouthwesternPublicServiceCo">Southwestern Public Service Co</Link></td>
                      <img className="map" src="https://powernet-resources-dev.s3.us-east-2.amazonaws.com/Maddox.png"/>
                    </tr>
                    <tr>
                      <td><Link to="/PowerPlants/MainCanalHeadworks">Main Canal Headworks</Link></td>
                      <td>26.8</td>
                      <td>47.6164</td>
                      <td>-119.2992</td>
                      <td>Hydro</td>
                      <td>1987</td>
                      <td><Link to="/Organizations/ColumbiaBasinHydropower">Columbia Basin Hydropower</Link></td>
                      <img className="map" src="https://powernet-resources-dev.s3.us-east-2.amazonaws.com/MainCanalHeadworks.png"/>
                    </tr>
                  </tbody>
                </Table>
            </header>
        </div>
    );
}
