import React from 'react';
import '../App.css';
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Policies() {
    return (
        <div className="App">
            <header className="App-header">
                <h1>Policies Overview Page</h1>
                <Table variant="dark">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>State</th>
                            <th>Website</th>
                            <th>Year Created</th>
                            <th>Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><Link to="/Policies/avistacommercialenergyefficiencyincentivesprogram">Avista Utilities (Electric) - Commercial Energy Efficiency Incentives Program</Link></td>
                            <td>Washington</td>
                            <td><a href="https://www.myavista.com/energy-savings/tools-for-your-business/rebates-washington">Link</a></td>
                            <td>2016</td>
                            <td>Rebate Program</td>
                        </tr>
                        <tr>
                            <td><Link to="/Policies/georgiaadvancedsolarinitiative">Georgia Power - Advanced Solar Initiative</Link></td>
                            <td>Georgia</td>
                            <td><a href="https://www.georgiapower.com/about-energy/energy-sources/solar/asi/advanced-solar-initiative.cshtml">Link</a></td>
                            <td>2015</td>
                            <td>Performance Based Incentive</td>
                        </tr>
                        <tr>
                            <td><Link to="/Policies/saintpaulpaceprogram">Saint Paul Port Authority PACE Program</Link></td>
                            <td>Minnesota</td>
                            <td><a href="http://sppa.com/financing-businesses/">Link</a></td>
                            <td>2015</td>
                            <td>PACE financing</td>
                        </tr>
                        <tr>
                            <td><Link to="/Policies/albuquerquesolareasementandrightslaws">City of Albuquerque - Solar Easements and Rights Laws</Link></td>
                            <td>New Mexico</td>
                            <td>N/A</td>
                            <td>2014</td>
                            <td>Solar/Wind Access Policy</td>
                        </tr>
                        <tr>
                            <td><Link to="/Policies/solarandwindenergydevicefranchisetaxdeduction">Solar and Wind Energy Device Franchise Tax Deduction</Link></td>
                            <td>Texas</td>
                            <td><a href="http://www.seco.cpa.state.tx.us/re/incentives-taxcode-statutes.php">Link</a></td>
                            <td>2000</td>
                            <td>Corporate Tax Deduction</td>
                        </tr>
                        <tr>
                            <td><Link to="/Policies/solarandwindenergybusinessfranchisetaxexemption">Solar and Wind Energy Business Franchise Tax Exemption</Link></td>
                            <td>Texas</td>
                            <td><a href="http://www.seco.cpa.state.tx.us/re/incentives-taxcode-statutes.php">Link</a></td>
                            <td>2000</td>
                            <td>Industry Recruitment/Support</td>
                        </tr>
                        <tr>
                            <td><Link to="/Policies/renewableenergysystemspropertytaxexemption">Renewable Energy Systems Property Tax Exemption</Link></td>
                            <td>Texas</td>
                            <td><a href="https://comptroller.texas.gov/taxes/property-tax/exemptions/">Link</a></td>
                            <td>2000</td>
                            <td>Property Tax Incentive</td>
                        </tr>
                    </tbody>
                </Table>
            </header>
        </div>
    );
}
