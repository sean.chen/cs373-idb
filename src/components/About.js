import React from 'react';
import logo from '../logo.svg';
import '../App.css';
import Link from 'react-router-dom/Link'
import {GitlabStats, MemberStats} from './stats'
import { Container, Row, Col, Card, Table } from 'react-bootstrap'
import holder from '../holder.png'
function About() {
  return (
    <div class="bg-dark">
      <Container fluid>
        <Row>
          <Col>
          <img src={logo} className="App-logo" alt="logo" />
          </Col>
        </Row>

      <Row>
        <Col>
        <h1 class="text-white">Description</h1>
        </Col>
      </Row>
      <Row>
        <Col>
        <p class="text-white">PowerNet is a website designed to educate citizens about energy companies all over the U.S so that people can make smarter, more informed, decisions regarding
        the energy consumption.In addition PowerNet provides information about legislative policies which may allow for citizens to take advantage of the benefits of switching to greener energy
        sources.</p>
        </Col>
      </Row>
      <Row>
        <Col>
        <h1 class="text-white">About Our Data</h1>
        </Col>
      </Row>
      <Row>
        <Col>
        <p class="text-white">The data we gather is centered on giving accurate information on energy infrastructure around the United States. The intersection of location, companies, and policies
        provides a great repsentation of any location's energy providers. The data provides insight into certain trends in places where different policies and companies meet.</p>
        </Col>
      </Row>
        <Row> 
          <Col>
          <h1 class="text-center text-white">Team Members</h1>
          </Col>
        </Row>

        <Row className="Users">

          <Col>
          <Card>
            <Card.Img variant="top" src="https://powernet-resources-dev.s3.us-east-2.amazonaws.com/alexgreason.jpg" />
            <Card.Body>
              <Card.Title>Alexander Greason</Card.Title>
              <Card.Subtitle class="text-muted">Fullstack Developer</Card.Subtitle>

              <Card.Text class="mt-1">
                I'm Alex, I am presently in my senior year, double-majoring in CS and Math.
                I am interested in renewable energy, space tech, semiconductor engineering,
                cellular automata and machine learning. My hobbies are reading fanfiction and programming.
              </Card.Text>
              <MemberStats email={"alexgreason@utexas.edu, quantum935@yahoo.com"} username="alexgreason1" />
            </Card.Body>
          </Card>
          </Col>
        
          <Col>
          <Card>
            <Card.Img variant="top" src="https://powernet-resources-dev.s3.us-east-2.amazonaws.com/harrison.jpg" />
            <Card.Body>
              <Card.Title>Harrison Gross</Card.Title>
              <Card.Subtitle class="text-muted">Fullstack Developer</Card.Subtitle>

              <Card.Text class="mt-1">
              I'm a senior computer science major.  In my spare time I like to play basketball and videogames.
              </Card.Text>
              <MemberStats email={"harrisongross17@gmail.com"} username="harriso17" />
            </Card.Body>
          </Card>
          </Col>

          <Col>
          <Card>
            <Card.Img variant="top" src="https://powernet-resources-dev.s3.us-east-2.amazonaws.com/sean.jpg"/>
            <Card.Body>
              <Card.Title>Sean Chen</Card.Title>
              <Card.Subtitle class="text-muted">Fullstack Developer</Card.Subtitle>

              <Card.Text class="mt-1">
                I'm a third year Computer Science student at UT Austin. Most of the time I spend outside of CS is in CSGO (and other video games).
              </Card.Text>
              <MemberStats email={"sean.chen@utexas.edu, sean@Seans-Mac-Pro.local"} username="sean.chen" />
            </Card.Body>
          </Card>
          </Col>

          <Col>
          <Card>
            <Card.Img variant="top" src="https://powernet-resources-dev.s3.us-east-2.amazonaws.com/travis.png" />
            <Card.Body>
              <Card.Title>Travis Eakin</Card.Title>
              <Card.Subtitle class="text-muted">Fullstack Developer</Card.Subtitle>

              <Card.Text class="mt-1">
              Hello! I'm Travis. I'm currently a senior majoring in Computer Science. I will be 
              working for Lockheed Martin post-graduation and I'm super excited about it. In my spare 
              time, I enjoy cycling and game development.
              </Card.Text>

              <MemberStats email={"travis.eakin@utexas.edu"} username="eakintk" />
            </Card.Body>
          </Card>
          </Col>

          <Col>
          <Card>
            <Card.Img variant="top" src="https://powernet-resources-dev.s3.us-east-2.amazonaws.com/cdesantis.jpeg"/>
            <Card.Body>
              <Card.Title>Carlo DeSantis</Card.Title>
              <Card.Subtitle class="text-muted">Fullstack Developer</Card.Subtitle>
              <Card.Text class="mt-1">
                My name is Carlo. I'm currently a junior here at UT. Outside of classes I like to spend my time watching movies or playing guitar.
              </Card.Text>
              <MemberStats email={"cdesantis@tanklogix.com, cdesantis@utexas.edu"} username="cdesan"/>
            </Card.Body>
          </Card>
          </Col>
        </Row>

        <Row><Col><h2 class="text-white text-center">Project Stats</h2></Col></Row>

        <Row>
          <Col>
            <GitlabStats />
          </Col>
        </Row>

        <Row><Col><h2 class="text-white text-left">Tools Used</h2></Col></Row>

        <Row>
          <Col>
            <Table variant="dark">
              <thead class="text-left">
                    <tr>
                    <th>Tool</th>
                    <th>Use</th>
                    </tr>
              </thead>
              <tbody class="text-left">
                <tr>
                    <td>Gitlab</td>
                    <td>We use Gitlab to host our source code for this project as well as utilize Gitlab's issue tracking to provide communication of tasks between team members.</td>
                </tr>
                <tr>
                    <td>Amazon Amplify</td>
                    <td>We use Amplify to host our website on the internet. </td>
                </tr>
                <tr>
                    <td>
                      React
                    </td>
                    <td>
                      We use React as the framework for basic functionaliy of our web app.
                    </td>
                </tr>
                <tr>
                    <td>
                      React Router
                    </td>
                    <td>
                      We use React router to provide routing for the web app.
                    </td>
                </tr>
                <tr>
                    <td>
                      React Player
                    </td>
                    <td>
                      We use React player to support embedded videos in various pages.
                    </td>
                </tr>
                <tr>
                  <td>
                    Bootstrap
                  </td>
                  <td>
                    We use Bootstrap as the styling framewwork of the site.
                  </td>
                  </tr>
                  <tr>
                    <td>
                      Postman
                    </td>
                    <td>
                      We use Postman for developing and testing out our own API.
                    </td>
                </tr>
              <tr>
                  <td>Slack</td>
                  <td>We use Slack for direct communication between team members via messaging.</td>
              </tr>
              <tr>
                  <td>Zoom</td>
                  <td>We use Zoom for out weekly team meetings.</td>
              </tr>
              <tr>
                  <td>Namecheap</td>
                  <td>We use to obtain our domain Powernet.energy.</td>
              </tr>
              </tbody>
          </Table>
          </Col>
        </Row>

        <Row><Col><h2 class="text-white text-left">Data Sources</h2></Col></Row>

        <Row>
          <Col>
            <Table variant="dark">
              <thead class="text-left">
                    <tr>
                    <th>Data Source</th>
                    <th>Scrape Method</th>
                    </tr>
              </thead>
              <tbody class="text-left">
                <tr>
                    <td><a href="https://www.eia.gov/opendata/commands.php">https://www.eia.gov/opendata/commands.php</a></td>
                    <td>Data aquired via RESTful API calls</td>
                </tr>
                <tr>
                    <td><a href="https://nsrdb.nrel.gov/data-sets/api-instructions.html">https://nsrdb.nrel.gov/data-sets/api-instructions.html</a></td>
                    <td>Data aquired via RESTful API calls</td>
                </tr>
                <tr>
                    <td><a href="https://developers.google.com/earth-engine/#api">https://developers.google.com/earth-engine/#api</a></td>
                    <td>Data aquired via RESTful API calls</td>
                </tr>
                <tr>
                    <td><a href="https://www.census.gov/data/developers/data-sets/popest-popproj/popest.html">https://www.census.gov/data/developers/data-sets/popest-popproj/popest.html</a></td>
                    <td>Data aquired via RESTful API calls</td>
                </tr>
              </tbody>
          </Table>
          </Col>
        </Row>
        <Row>
          <Col>
            <a class="text-white" href="https://gitlab.com/harriso17/cs373-idb"><h3>Gitlab Repo</h3></a>
          </Col>
          <Col>
          <a class="text-white" href="https://documenter.getpostman.com/view/12849192/TVRdAXQX"><h3>Postman Docs</h3></a>
          </Col>
        </Row>
      </Container>
    </div>

  );
}

export default About;

