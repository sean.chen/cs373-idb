import React from 'react';
import logo from '../logo.svg';
import './About.css';
import NavBar from '../components/NavBar';

function About() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>About Page</h1>
        <img src={logo} className="App-logo" alt="logo" />
      </header>
    </div>

  );
}

export default About;

