## Members
Travis Eakin (tke246 | @eakintk)
Carlo Desantis (ccd2238 | @cdesan)
Harrison Gross (rhg555 | @harriso17)
Sean Chen (sc53864 | @sean.chen)
Alexander Greason (abg2352 | @alexgreason1)

## Git SHA
a3cf21c0

## Project Leader
Collective

## GitLab Pipelines
[https://gitlab.com/harriso17/cs373-idb/-/pipelines](https://gitlab.com/harriso17/cs373-idb/-/pipelines)

## Website
[powernet.energy](https://www.powernet.energy/)

## Estimated Time and Actual Time (in hours)
Travis Eakin - estimated: 24 actual: 10
Carlo Desantis - estimated: 5 actual: 10
Harrison Gross - estimated: 5 actual: 10
Sean Chen - estimated: 20 actual: 15
Alexander Greason - estimated: 20 actual: 15

## Comments
